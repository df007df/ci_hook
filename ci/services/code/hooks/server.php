<?php
namespace Ci\Services\Code\Hooks;

/**
 * Description of base
 *
 * @author df
 */
class Server
{


    protected $_hookRequestData = null;


    protected $_attribute = [];


    function __construct() {
        ;
    }


    /**
     * 获取解析的hook class
     */
    public static function factory($hook_type, $request = [])
    {
        $hook = self::_newHook($hook_type);
        if (empty($request)) {
            $request = $hook->_parseBitBucketRequest();
        }

        $hook->setRequest($request);
        $hook->_parseRequest();
        return $hook;
    }


    private static function _newHook($hook_type)
    {
        static $cache = [];

        if (empty($cache[$hook_type])) {
            $className = 'Ci\Services\Code\Hooks\\' . ucfirst($hook_type);
            $cache[$hook_type] = new $className();
        }

        return $cache[$hook_type];
    }


    function setRequest($request = [])
    {
        $this->_hookRequestData = $request;
    }

    function getRequest()
    {
        return $this->_hookRequestData;
    }

    /**
     * 解析bitbucket hook请求入口
     */
    protected function _parseBitBucketRequest()
    {

        if (!empty($_POST['payload'])) {
            $json = str_replace('\"', '"', $_POST['payload']);
            return json_decode($json, true);
        }
        return [];
    }


    protected function _parseRequest()
    {
        $data = $this->getRequest();
        return !empty($data) ? $data : null;
    }




    ################## 代码推送 操作###############

    /**
     * fetch 远程 branch
     */
    function repositoryFetch($remote, $branch)
    {


    }




    /**
     * 自动属性
     * @param type $name
     * @param type $arguments
     */
    function __call($name, $arguments) {

    }

    function __set($name, $value = null) {
        $this->_attribute[$name] = $value;
    }

    function __get($name) {
        if (array_key_exists($name, $this->_attribute)) {
            return $this->_attribute[$name];
        } else {
            return null;
        }
    }

}
