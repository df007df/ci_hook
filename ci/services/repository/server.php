<?php
namespace Ci\Services\Repository;

/**
 * 代码仓库工具
 *
 * @author df
 */
class Server
{

    const GIT_USER = 'git';

    private $_remote = 'origin';

    private $_gitPath = '';  //git代码仓库dir

    function __construct($remote = 'origin') {
        $this->_remote = $remote;
    }


    function setPath($path)
    {
        $this->_gitPath = $path;
    }

    function getPath()
    {
        return $this->_gitPath;
    }

    function getRemote()
    {
        return $this->_remote;
    }


    function setRemote($remote)
    {
        $this->_remote = $remote;
    }


    function fetch($branch)
    {
        if (empty($branch)) {
            return false;
        }
        $command = "git --bare fetch {$this->_remote} $branch:$branch";
        return $this->_exec($command);
    }



    function push($branch)
    {
        if (empty($branch)) {
            return false;
        }
        $command = "git push {$this->_remote} $branch:$branch -f";
        return $this->_exec($command);
    }






    protected function _exec($command = '')
    {
        if (!empty($command)) {
            $oldPath = getcwd();
            $gitpath = $this->getPath();
            chdir($gitpath);
            exec($command, $oupt, $status);
            chdir($oldPath);
            return $status;
        } else {
            false;
        }
    }

}
