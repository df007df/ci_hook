<?php
include_once 'ci/init.php';


$postdata = array (
  'repository' =>
  array (
    'website' => '',
    'fork' => false,
    'name' => 'df007df',
    'scm' => 'git',
    'owner' => 'df007df',
    'absolute_url' => '/df007df/df007df/',
    'slug' => 'df007df',
    'is_private' => false,
  ),
  'truncated' => false,
  'commits' =>
  array (
    0 =>
    array (
      'node' => '473d0ee82b3b',
      'files' =>
      array (
        0 =>
        array (
          'type' => 'added',
          'file' => 'sdsd',
        ),
      ),
      'raw_author' => 'df ',
      'utctimestamp' => '2014-09-15 01:16:23+00:00',
      'author' => 'df007df',
      'timestamp' => '2014-09-15 03:16:23',
      'raw_node' => '473d0ee82b3b5dca2f0003df8bf5a81f0e73c1f1',
      'parents' =>
      array (
        0 => '870f6880c47f',
      ),
      'branch' => 'dev1',
      'message' => 'add ssd\\n',
      'revision' => NULL,
      'size' => -1,
    ),
  ),
  'canon_url' => 'https://bitbucket.org',
  'user' => 'df007df',
);


ob_start();

var_export(json_decode($_POST['payload'], true));

$pp = ob_get_clean();
Ci\log($pp);

$post = Ci\Services\Code\Hooks\Server::factory('post');

//fetch branch
$repository = new Ci\Services\Repository\Server(config('remote'));
$repository->setPath(config('gitpath'));

$branch = $post->branch;
if (empty($branch)) {
    return;
}

Ci\log($branch . ' fetch start...');
$return = $repository->fetch($branch);
Ci\log($branch . ' fetch status:' . $return);

Ci\log($branch . ' push start...');
$repository->setRemote($branch);
$return = $repository->push($branch);
Ci\log($branch . ' push status:' . $return);
